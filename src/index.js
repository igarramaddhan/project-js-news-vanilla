import {NEWS_API_KEY} from '../env';

// @flow

const createElementWithAttributes = (
  element: string,
  attributes: {name: string, value: any}[]
) => {
  const el = document.createElement(element);
  attributes.forEach(val => el.setAttribute(val.name, val.value));
  return el;
};

const createTextElement = (element: string, text: string) => {
  const el = document.createElement(element);
  el.appendChild(document.createTextNode(text));
  return el;
};

const convertNullable = (text: string | null) => (text ? text : '');

const root = document.getElementById('root');
/** Header */
const header = createElementWithAttributes('div', [
  {name: 'class', value: 'header'},
]);
const text = createTextElement('h3', 'News App');
header.appendChild(text);

root && root.appendChild(header);

const contentContainer = createElementWithAttributes('div', [
  {name: 'class', value: 'container'},
]);
const contentText = createTextElement('p', 'Top Headlines');
contentText.setAttribute('class', 'instruction');
contentContainer.append(contentText);

type Item = {
  urlToImage: string,
  url: string,
  title: string,
  author: string | null,
  source: {id: string | null, name: string | null},
};

const createItem = (item: Item) => {
  const itemContainer = createElementWithAttributes('div', [
    {name: 'class', value: 'news-list-item'},
  ]);

  const image = createElementWithAttributes('img', [
    {name: 'src', value: item.urlToImage},
  ]);
  itemContainer.appendChild(image);

  const itemContentContainer = createElementWithAttributes('div', [
    {name: 'class', value: 'news-list-item-content-container'},
  ]);

  const itemTitle = createElementWithAttributes('a', [
    {name: 'href', value: item.url},
    {name: 'target', value: '__blank'},
  ]);

  itemTitle.appendChild(document.createTextNode(item.title));

  itemContentContainer.appendChild(itemTitle);

  const author = createTextElement(
    'span',
    `by ${convertNullable(item.author)}`
  );
  itemContentContainer.appendChild(author);

  const source = createTextElement(
    'p',
    `Source: ${convertNullable(item.source.name)}`
  );
  itemContentContainer.appendChild(source);

  itemContainer.appendChild(itemContentContainer);
  contentContainer.appendChild(itemContainer);
};

const populateData = () => {
  fetch(
    `https://newsapi.org/v2/top-headlines?country=us&apiKey=${NEWS_API_KEY}`
  )
    .then(res => res.json())
    .then(res => {
      if (res.status !== 'error') {
        res.articles.forEach(val => createItem(val));
      }
    });
};

populateData();

root && root.appendChild(contentContainer);
